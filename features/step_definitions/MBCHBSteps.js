//taskkill /im chromedriver.exe /f

var data = require('../../TestResources/GlobalTestData.js');
var Objects = require(__dirname + '/../../repository/MBCHBPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
//var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var http=require('http');
//var err;
//var email = require(__dirname + '/../../node_modules/emailjs');

var LoginPage, DashboardPage, ForgotPage;
var CurrentUrl="";

function initializePageObjects(browser, callback) {
    var MBCPage = browser.page.MBCHBPages();
    LoginPage = MBCPage.section.LoginPage;
    DashboardPage= MBCPage.section.DashboardPage;
    AdminPage=MBCPage.section.AdminPage;
    ForgotPage=MBCPage.section.ForgotPage;
    callback();
}

module.exports = function() {

    //Default Credentials:

    this.Given(/^User launches the MBC login page in Browser$/, function () {
    var URL;
    browser = this;
    var execEnv = data["TestingEnvironment"];
    console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAI") {
            URL = data.MBCURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                browser.pause(data.longWait)
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                browser.pause(data.longWait);
                LoginPage.waitForElementVisible('@inputUsername', data.longWait);
                browser.pause(data.longWait);
            });
        }
    });

    this.When(/^User enters default username password and clicks on Submit$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
                .setValue('@inputUsername', data.MBCDUsername)
                .setValue('@inputPassword', data.MBCDPassword)
                .click('@SubmitButton');
            browser.pause(data.longWait);
            browser.pause(60000)
        }
    });

    this.Then(/^User should see error message in login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            LoginPage.waitForElementVisible('@DefaultLoginError', data.longWait)
            browser.pause(data.longWait);
        }
    });

    //ByPassing:

    this.Given(/^Admin logs in and navigates to Administration page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
                .setValue('@inputUsername', data.MBCAUsername)
                .setValue('@inputPassword', data.MBCAPassword)
                .click('@SubmitButton');
            browser.timeoutsImplicitWait(60000)
            browser.pause(data.longWait)
            DashboardPage.waitForElementVisible('@DashboardTab', data.longWait)
            browser.pause(30000)
            DashboardPage.waitForElementVisible('@AdminTab', data.longWait)
                .click('@AdminTab');
            browser.timeoutsImplicitWait(30000)
            DashboardPage.waitForElementVisible('@EmpTab', data.longWait)
                .click('@EmpTab');
            browser.timeoutsImplicitWait(30000)
            DashboardPage.waitForElementVisible('@Keyword', data.longWait)
            browser.timeoutsImplicitWait(30000)
            browser.pause(data.longWait)
        }

    });

    this.When(/^Current page URL of Administration page is stored$/, function () {
        var URL;

        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {

            browser.timeoutsImplicitWait(30000)

            browser.url(function (urlvalue) {
                // returns the current url
                CurrentUrl = urlvalue;
                console.log(CurrentUrl);
                browser.timeoutsImplicitWait(10000)
               // browser.pause(data.longWait);
            });
        }
    });


    this.Given(/^Admin logs out of MBC$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            browser.pause(10000)
            DashboardPage.waitForElementVisible('@Keyword', data.longWait)
            browser.pause(10000)
            DashboardPage.waitForElementVisible('@Logout', data.longWait)
                .click('@Logout');
            browser.timeoutsImplicitWait(30000)
            browser.pause(30000)
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)

        }
    });


    this.Given(/^User enters username and password and clicks on Submit$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
                .setValue('@inputUsername', data.MBCUsername)
                .setValue('@inputPassword', data.MBCPassword)
                .click('@SubmitButton');
            browser.pause(60000)
            DashboardPage.waitForElementVisible('@DashboardTab', data.longWait)
            browser.pause(30000)

        }
    });

    this.Given(/^User launches the copied Admin URL$/, function () {
        var url;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            //  url=CurrentUrl;
            browser.pause(data.longWait)
            console.log(CurrentUrl.value);
            browser.url(CurrentUrl.value);
            //browser.url('{url}', function(result) {
            //    console.log(result);
//
            //});

        }
    });


    this.Given(/^User should not navigate to Administration page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            browser.pause(30000);
//User should be on the same dashboard page and will not be able to navigate to admin page
            DashboardPage.waitForElementVisible('@DashboardTab', data.longWait)
            browser.pause(60000)
            DashboardPage.waitForElementVisible('@Logout', data.longWait)
                .click('@Logout');
            browser.timeoutsImplicitWait(30000)
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
            browser.pause(5000)


        }
    });


    this.Given(/^User changes https to http$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            URL = data.httpUrl;
            browser.pause(5000)
            initializePageObjects(browser, function () {
                browser.maximizeWindow();
                browser.timeoutsImplicitWait(10000)
                    .url(URL);
                browser.pause(data.longWait)
                console.log(URL);
                browser.pause(5000)


            });
        }
    });

    this.Given(/^User should be redirected to https login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            URL = data.MBCURL;
            browser.pause(60000)
            browser.assert.urlEquals(URL);
            browser.pause(data.longWait)
        }
    });

    //WeakLockOut

    this.When(/^User enters invalid username and password and clicks Submit$/, function () {
        var URL;
        var a;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            a = 0;

            if (a < 3) {

                LoginPage.waitForElementVisible('@inputPassword', data.longWait)

                if (a == 0) {
                    LoginPage.waitForElementVisible('@inputPassword', data.longWait).setValue('@inputUsername', data.MBCIUsername)
                        .setValue('@inputPassword', data.MBCIPassword)
                        .click('@SubmitButton');
                    browser.pause(10000)
                    LoginPage.waitForElementVisible('@CredsError', data.longWait)
                    browser.pause(10000)
                    a = a + 1;
                }

                if (a == 1) {
                    LoginPage.waitForElementVisible('@inputPassword', data.longWait).setValue('@inputUsername', data.MBCIIUsername)
                        .setValue('@inputPassword', data.MBCIIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.longWait)
                    LoginPage.waitForElementVisible('@CredsError', data.longWait)
                    browser.pause(data.longWait)
                    a = a + 1;
                }

                if (a == 2) {
                    LoginPage.waitForElementVisible('@inputPassword', data.longWait).setValue('@inputUsername', data.MBCIIIUsername)
                        .setValue('@inputPassword', data.MBCIIIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.longWait)
                    ForgotPage.waitForElementVisible('@ForgotPageCancel', data.longWait)
                    browser.pause(data.longWait)
                }
            }
        }
    });

    this.When(/^Clicks Cancel from Forgot Username or Password page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            browser.pause(60000)
            ForgotPage.waitForElementVisible('@ForgotPageCancel', data.longWait)
                .click('@ForgotPageCancel');
            browser.pause(60000)
             browser.acceptAlert();
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
            browser.pause(60000)
        }

    });

    this.When(/^User enters invalid username and password and clicks Submit again$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {

            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
                .setValue('@inputUsername', data.MBCIVUsername)
                .setValue('@inputPassword', data.MBCIVPassword)
                .click('@SubmitButton');
            browser.pause(data.longWait)
            LoginPage.waitForElementVisible('@CredsError', data.longWait)
            browser.pause(data.longWait)

        }
    });

    this.Then(/^User should see account lockedout message in login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            browser.pause(data.longWait);
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
                .setValue('@inputUsername', data.MBCVUsername)
                .setValue('@inputPassword', data.MBCVPassword)
                .click('@SubmitButton');
            browser.pause(data.longWait)
            browser.timeoutsImplicitWait(30000);
            LoginPage.waitForElementVisible('@LockError', data.longWait)
            browser.timeoutsImplicitWait(30000)
            browser.pause(data.longWait)
        }

    });

//ErrorMessage:

    this.When(/^User enters invalid username and password and clicks on Submit$/, function () {

        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
                .setValue('@inputUsername', data.MBCIUsername)
                .setValue('@inputPassword', data.MBCIPassword)
                .click('@SubmitButton');
            browser.pause(10000)
        }

    });


//NoCookies

    this.When(/^User logsout of MBC and tries to navigate back$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {

            browser.timeoutsImplicitWait(30000)
            DashboardPage.waitForElementVisible('@Logout', data.longWait)
                .click('@Logout');
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(30000)
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
            browser.pause(data.longWait)
               browser.back();
            browser.pause(30000)


        }
    });

    this.Then(/^User lands back on Login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAI") {

            browser.timeoutsImplicitWait(10000)
            LoginPage.waitForElementVisible('@inputPassword', data.longWait)
             browser.pause(5000)
        }
    });

//ClickJacking Scenario

    this.Given(/^User creates an html file$/,function(){
        var URL;
        browser=this;
        var execEnv=data["TestingEnvironment"];
        if(execEnv.toUpperCase()=="QAI"){
            //fs.createFileFromStorageFile()
            fs.writeFile("C:\\Users\\rachana-gn\\Documents/Script.html",
                "<html>" +
                "<head>" +
                "<title>test</title>" +
                "</head>" +
                "<body>" +
                "<p>This website is vulnerable to Clickjacking</p>" +
                "<iframe height=500 width=1000 scrolling=yes frameborder=1 marginwidth=10 src='https://auth-qai.mercerbenefitscentral.com/mbcqa3/login.tpz'>" +
                "</body>" +
                "</html>"
                ,function(err){
            if(err){
                returnconsole.log(err);
            }

        });
    }
});

    this.When(/^User tries to open the file$/,function(){
        var URL;
        browser=this;
        var execEnv=data["TestingEnvironment"];
        if(execEnv.toUpperCase()=="QAI"){
            console.log("The file was saved!");
            var fileURL;
            fileURL='\'../../TestResources/Script.html\'';
            //fileURL='C:\\Users\\rachana-gn\\Documents/Script.html'
            initializePageObjects(browser,function(){
                browser.timeoutsImplicitWait(50000)
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(fileURL);
                // browser.timeoutsImplicitWait(90000);
                // browser.timeoutsImplicitWait(90000);
                browser.assert.elementNotPresent('@inputPassword');
                browser.pause(30000)

            });
        }
    });
}


