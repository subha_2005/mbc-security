Feature: SQLinjection

  @MBCSecurity @Passwordtextfeild
  Scenario: To test that password text field does not accept SQL Queries
    Given User launches the MBC Application in Browser
    When  User enters the username in Username textbox
    And   User enters the SQL Query as password in the Password textbox
    And   User clicks on submit
    Then  Verify error message for wrong Username and Password

  @MBCSecurity @SSNtextfield
  Scenario: To test that SSN textfield does not accept SQL Queries
    Given User launches the MBC Application in Browser
    When User enters the username in Username textbox
    And User enters the password in the Password textbox
    And User clicks on submit
    When User clicks on Menu
    And User clicks on Complete Life Event
    And User clicks on Get Started Tab
    And User navigates to Who's covered page
    Then User clicks on Add Dependent Button
    And User clicks on Continue Button
    And User enters the SQL Query in SSN textfield
    And Verify error message when entered wrong SSN
    And User logs out of MBC Application

  @MBCSecurity @MyInformationPage
  Scenario: To test that ContactField does not accept SQL Queries
    Given User launches the MBC Application in Browser
    When User enters the username in Username textbox
    And User enters the password in the Password textbox
    And User clicks on submit
    When User clicks on Menu
    And User clicks on Complete Life Event
    And User clicks on Get Started Tab
    And User navigates to Who's covered page
    Then User navigates to My Information page
    And User clicks on edit button MyInfoPage
    And User enters SQL query in Mobile phone text field
    And Verify error message for entering wrong Mobile number
    And User logs out of MBC Application

  @MBCSecurity @CrossSite
  Scenario: To test whether an alert popup is displayed on entering the script in editable fields
    Given User launches the MBC Application in Browser
    When User enters the username in Username textbox
    And User enters the password in the Password textbox
    And User clicks on submit
    When User clicks on Menu
    And User clicks on Profile link
    And User clicks on edit button in Profile page
    Then User enters the data in the editable field
    And Verifies the alert popup
    And User logs out of MBC Application

#Then User shouldnt see the content of the file in the browse
