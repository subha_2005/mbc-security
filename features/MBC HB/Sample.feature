Feature: Security Testing for Login pages

  @MBCSecurity @DefaultCredentials
  Scenario: To verify that login is unsuccessful for a returning user with default credential
  Given  User launches the MBC login page in Browser
  When   User enters default username password and clicks on Submit
  Then   User should see error message in login page

  @MBCSecurity @ByPassing
  Scenario: To validate that MBC Application shouldnot be accessible with unauthorized Admin URL
    Given User launches the MBC login page in Browser
    When  Admin logs in and navigates to Administration page
    And   Current page URL of Administration page is stored
    And   Admin logs out of MBC
    And   User enters username and password and clicks on Submit
    And   User launches the copied Admin URL
    Then  User should not navigate to Administration page


  @MBCSecurity @HTTPSecurity
  Scenario: To check that MBC Login page should not be accessible with http
    Given  User launches the MBC login page in Browser
    When   User changes https to http
    Then   User should be redirected to https login page

  @MBCSecurity @WeakLockOut
  Scenario: To check that the user account gets locked out on attempting invalid logins morethan a specific number of times
    Given  User launches the MBC login page in Browser
    When   User enters invalid username and password and clicks Submit
    And    Clicks Cancel from Forgot Username or Password page
    And    User enters invalid username and password and clicks Submit again
    Then   User should see account lockedout message in login page

  @MBCSecurity @ErrorMessage
  Scenario: To verify that error message displays on using invalid credentials
    Given  User launches the MBC login page in Browser
    When   User enters invalid username and password and clicks on Submit
    Then   User should see error message in login page


  @MBCSecurity @NoCookies

  Scenario: To validate that site doesnt have any cookies stored to load dashboard after logging out from the site
    Given User launches the MBC login page in Browser
    And User enters username and password and clicks on Submit
    When  User logsout of MBC and tries to navigate back
    Then User lands back on Login page

 @MBCSecurity @ClickJacking

 Scenario: To validate if the site is vulnerable
#Then User shouldnt see the content of the file in the browser

   Given User creates an html file
   When User tries to open the file
   #Then User shouldnt see the content of the file in the browse





























