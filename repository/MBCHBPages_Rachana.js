/**
 * Created by Rachana on 12/12/2017.
 * Updated by Rachana on 03/01/2018
 */





module.exports = {
    sections: {
        LoginPage: {
            selector: 'body',
            elements: {
                Username: {locateStrategy: 'xpath',selector: "//input[@id='usernameId']"},
                Password: {locateStrategy: 'xpath',selector: "//input[@id='passwordId']"},
                SubmitButton: {locateStrategy: 'xpath',selector: "(//input[@type='submit'])[1]"},
                MenuButton: {locateStrategy: 'xpath',selector: "//span[text()='Menu']"},
                CompleteLifeEventButton: {locateStrategy: 'xpath',selector: "//span[.='Complete Life Event']"},
                GetStartedTab: {locateStrategy: 'xpath',selector: "(//a[@class='ng-scope ng-binding'])[1]"},
                WhosCoveredTab: {locateStrategy: 'xpath',selector: "//button[.='NEXT']"},
                AddDependentButton: {locateStrategy: 'xpath',selector: "//span[.='Add a Dependent']"},
                ContinueButton: {locateStrategy: 'xpath',selector: "(//button[.='Continue'])[2]"},
                SSN: {locateStrategy: 'xpath',selector: "//input[@name='SSN']"},
                Random: {locateStrategy: 'xpath',selector: "(//button[@name='Gender'])[2]"},
                ContinueButtoninWCP: {locateStrategy: 'xpath',selector: "//span[.='Continue']"},
                EditButtoninWCP: {locateStrategy: 'xpath',selector: "//span[.='Edit']"},
                Mobiletextfield: {locateStrategy: 'xpath',selector: "(//input[@name='phone'])[2]"},
                Random1: {locateStrategy: 'xpath',selector: "(//input[@name='phone'])[3]"},
                EstimateNow: {locateStrategy: 'xpath',selector: "(//span[.='ESTIMATE NOW'])[1]"},
                Profilepage: {locateStrategy: 'xpath',selector: "//a[@title='Profile']"},
                EditbuttoninProfilepage: {locateStrategy: 'xpath',selector: "//span[.='Edit']"},
                Addressfield: {locateStrategy: 'xpath',selector: "(//input[@name='AddressField'])[2]"},
                Random3: {locateStrategy: 'xpath',selector: "(//input[@name='AddressField'])[1]"},
                NextButton:{locateStrategy: 'xpath', selector: "//button[.='NEXT']"},
                LogoutButton: {locateStrategy: 'xpath',selector: "//a[text()='Logout']"}
            }
        }
    }};

